#!/usr/bin/env python3
import os
import sys

### BEGIN SETTINGS ###
from django.conf import settings

DEBUG = os.environ.get('DEBUG', 'on') == 'on'

SECRET_KEY = os.environ.get('SECRET_KEY', os.urandom(32))

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

MEDIA_ROOT = os.path.join(BASE_DIR, 'files')

settings.configure(
    DEBUG=DEBUG,
    SECRET_KEY=SECRET_KEY,
    BASE_DIR=BASE_DIR,
    ROOT_URLCONF='trappit.urls',
    MEDIA_ROOT=MEDIA_ROOT,
    MIDDLEWARE_CLASSES=(
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ),
    INSTALLED_APPS=(
        'django.contrib.admin',
        'django.contrib.admindocs',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'trappit',
        'taggit',
    ),
    MEDIA_URL='/media/',
    STATIC_URL='/static/',
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'trappit.db',
        }
    },
    TAGGIT_CASE_INSENSITIVE=True,
    TEMPLATES=[
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': ["trappit/"],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        }
    ],
    STATICFILES_DIRS=[
        os.path.join(BASE_DIR, "static"),
    ],
    LOGIN_REDIRECT_URL="/",
    LOGOUT_REDIRECT_URL="/",
)
### END SETTINGS  ###

### START WSGI ###
from django.core.wsgi import get_wsgi_application

apllication = get_wsgi_application()
### END WSGI

### START MANAGER ###
if __name__ == "__main__":
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

### END MANAGER ###


