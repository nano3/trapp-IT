from django.db import models

from taggit.managers import TaggableManager


class Specie(models.Model):
    """
    Model for a specie
    """
    name = models.CharField(max_length=256, unique=True, verbose_name="Nombre")
    thumbnail = models.ImageField(upload_to="files/", verbose_name="Icono")
    nombre_cientifico = models.CharField(max_length=256, verbose_name="Nombre científico")
    wikipedia_link = models.CharField(max_length=256, verbose_name="Enlace wikipedia")
    attributions = models.CharField(max_length=256, null=True, blank=True, verbose_name="Atribuciones icono")

    def __str__(self):
        return self.name


class VideoSource(models.Model):
    """
    Model for a video source
    """
    specie = models.ForeignKey(Specie, verbose_name="Especie")
    title = models.CharField(max_length=256, verbose_name="Título")
    description = models.TextField(max_length=1024, verbose_name="Descrición")
    source_file = models.FileField(upload_to="files/", verbose_name="Ficheiro")
    tags = TaggableManager(verbose_name="Etiquetas")
    created_at = models.DateTimeField("Fecha y hora de captura")
    id_camara = models.CharField(max_length=32, verbose_name="ID de Cámara")
    ayuntamiento_camara = models.CharField(max_length=256, verbose_name="Ayuntamiento donde se sitúa la cámara")
    localizacion_camara = models.CharField(max_length=256, verbose_name="Localización de la camara según retícula")
    tipo_habitat = models.CharField(max_length=256, verbose_name="Tipo de habitat")
    grupo_faunistico = models.CharField(max_length=256, verbose_name="Grupo Faunístico")
    numero_ejemplares = models.IntegerField(verbose_name="Número de ejemplares")
    notas_adicionales = models.TextField(max_length=1024, verbose_name="Notas Adicionales")

    def __str__(self):
        return self.title

    def as_timeline_json(self):
        year = self.created_at.year
        month = self.created_at.month
        day = self.created_at.day
        thumbnail_url = self.specie.thumbnail.url
        video_url = "/video/%s/" % (str(self.id))
        start_date = "%s/%s/%s" % (year, month, day)
        content = '<a href="%s"><img class="thumbnail" style="height: 88px;" src="%s"></img></a>' % (video_url, thumbnail_url)
        return dict(
            content=content,
            start=start_date,
        )
            

