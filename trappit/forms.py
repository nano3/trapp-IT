from django import forms
from django.forms import ModelForm
from .models import VideoSource, Specie


class VideoSourceForm(ModelForm):

    class Meta:
        model = VideoSource
        fields = [
            'title', 'description', 'source_file', 'tags', 'created_at',
            'id_camara', 'ayuntamiento_camara', 'localizacion_camara',
            'tipo_habitat', 'grupo_faunistico', 'numero_ejemplares',
            'notas_adicionales',
        ]


class SpecieForm(ModelForm):

    class Meta:
        model = Specie
        fields = ['name', 'thumbnail', 'nombre_cientifico', 'wikipedia_link', 'attributions']


class SearchSpecieForm(forms.Form):

    query = forms.CharField(max_length=256)
