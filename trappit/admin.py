from django.contrib import admin
from .models import VideoSource, Specie


class SourceAdmin(admin.ModelAdmin):
    pass


admin.site.register(VideoSource, SourceAdmin)
admin.site.register(Specie, SourceAdmin)
