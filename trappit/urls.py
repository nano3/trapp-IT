from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views

from . import views


urlpatterns = [
    # Admin related URLs
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),
    # Auth views
    url(r'^login$', auth_views.login, {"template_name": "templates/login.html"}, name="login"),
    url(r'^logout/$', auth_views.logout, name="logout"),
    # Trapp-IT related URLs
    url(r'^especies/nova/$', views.new_specie, name="nova_especie"),
    url(r'^especies/(?P<specie_id>\d+)/novo_video$', views.upload_video, name="novo_video"),
    url(r'^especies/(?P<specie_id>\d+)/$', views.specie_media, name="videos_especie"),
    url(r'^especies/$', views.species, name="especies"),
    url(r'^video/(?P<video_id>\d+)/$', views.view_video, name="ver_video"),
    url(r'^timeline/json/$', views.timeline_json, name="timeline_json"),
    url(r'^timeline/$', views.timeline, name="timeline"),
    url(r'^$', views.index, name="benvida"),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

