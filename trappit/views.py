import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from .forms import VideoSourceForm, SpecieForm, \
    SearchSpecieForm
from .models import Specie, VideoSource


def index(request):
    return render(
        request,
        "templates/index.html",
        {},
    )


@login_required
def upload_video(request, specie_id):

    title = "Novo ficheiro"
    
    if request.method == "POST":
        form = VideoSourceForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            specie = get_object_or_404(Specie, id=specie_id)
            instance.specie = specie
            instance.save()
            form.save_m2m()
            messages.success(request, "El ficheiro está arquivado")
            return redirect("especies")
    else:
        form = VideoSourceForm()

    return render(
        request,
        "templates/novo_video.html",
        {
            "title": title,
            "form": form
        }
    )


def species(request):

    title = "Especies"

    species = Specie.objects.all().order_by("name")

    if 'query' in request.GET:
        form = SearchSpecieForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            species = species.filter(name__icontains=query)
    else:
        form = SearchSpecieForm()
        
    return render(
        request,
        "templates/especies.html",
        {
            "title": title,
            "species": species,
            "form": form,
        }
    )


@login_required
def new_specie(request):

    title = "Nova especie"

    if request.method == "POST":
        form = SpecieForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "La especie se ha creado correctamente")
            return redirect("especies")
    else:
        form = SpecieForm()

    return render(
        request,
        "templates/nova_especie.html",
        {
            "title": title,
            "form": form,
        }
    )


def specie_media(request, specie_id):

    instance = get_object_or_404(Specie, id=specie_id)

    title = "Videos de %s" % (instance.name)

    videos = instance.videosource_set.all().order_by("-created_at")

    return render(
        request,
        "templates/videos_especie.html",
        {
            "title": title,
            "videos": videos,
            "instance": instance,
        }
    )


def view_video(request, video_id):

    video = get_object_or_404(VideoSource, id=video_id)

    title = video.title

    return render(
        request,
        "templates/ver_video.html",
        {
            "title": title,
            "video": video,
        }
    )


def timeline(request):

    title = "Timeline"

    return render(
        request,
        "templates/timeline.html",
        {
            "title": title,
        }
    )


def timeline_json(request):
    
    queryset = VideoSource.objects.all()
    results = [obj.as_timeline_json() for obj in queryset]
    return HttpResponse(json.dumps(results), content_type="application/json")
