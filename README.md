# README.md

This prototype was developed in two days during Ateneo Atlántico de Prototipado 2016 @ Domus Makers, A Coruña. The idea is to approach social colelctives like [GNHabitat](http://gnhabitat.org) and offer them a tech-based solution to their media archive. More specifically in what they call "fototrampeo" - a in-the-wild picture capture system for documenting local fauna.


## Dependencies

* python3
* python3-dev
* python3-venv
* python3-pip
* git

## Install for development

### Clone this repo

```
git clone https://gitlab.com/nano3/trapp-IT.git
```

### Create de virtual environment

```
cd trapp-IT
pyvenv env
```

### Install app requirements

```
pip3 install --upgrade requirements.txt
```

### Run migrations

```
./trap_it.py migrate
```

### Start development server

```
./trap_it.py runserver
```


